.. _introduction:

Introduction
============

This reference is to serve as a public compendium of information about the methods and members available via the API. 

Premiere Pro provides an ExtendScript-based API, allowing for broad control of the entire application. ExtendScript can access and manipulation of most project elements, including metadata, exporting and rendering options.

Adobe wants your integration to succeed; please don't hesitate to `contact us <mailto:bbb@adobe.com?subject=API_Question_From_Docs>`_ with questions, problems, or feature requests.